const books = ['Git Pocket Guide', 'Speaking JavaScript']

describe('', () => {
    beforeEach(() => {

        cy.visit(Cypress.config().baseUrl + Cypress.config().loginUrl)
            .title().should('have','ToolsQA')
            .login(Cypress.config().username, Cypress.config().password)
            .title().should('have','ToolsQA')

    })

    it('add books', () => {
        for (let book in books){
            cy.visit(Cypress.config().baseUrl + Cypress.config().booksUrl)
                .contains(books[book]).click()
                .contains('Add To Your Collection').click()
                .visit(Cypress.config().baseUrl + Cypress.config().profileUrl)
                .contains(books[book])
                .should('be.visible')
        }
        
    })

    it('delete all books', () => {
        cy.visit(Cypress.config().baseUrl + Cypress.config().profileUrl)
            .contains('Delete All Books').click()
            .get('#closeSmallModal-ok').click()
            .visit(Cypress.config().baseUrl + Cypress.config().profileUrl)
            .contains('No rows found')
            .should('be.visible')
    })

    afterEach(() => {
        cy.visit(Cypress.config().baseUrl + Cypress.config().loginUrl)
            .contains('Log out').click()
            .contains('Login').should('be.visible')
    })
})