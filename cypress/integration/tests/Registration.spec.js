const filepath = 'testData/test.png'

describe('registration', () => {

    it('register', () => {
        cy.visit(Cypress.config().baseUrl + Cypress.config().formUrl)
            .get('#firstName').type('Test')
            .get('#lastName').type('Test')
            .get('#userEmail').type('test@abc.com')
            .get('#genterWrapper > .col-md-9 > :nth-child(1)').click()
            .get('#userNumber').type('33333333')     
            .get('input[type="file"]').attachFile(filepath)
            .get('#submit').click()
            .get('.modal-body').should('be.visible')
    })   

    it('Assert if First name is a mandatory field', () => {
        cy.visit(Cypress.config().baseUrl + Cypress.config().formUrl)
            .get('#lastName').type('Test')
            .get('#userEmail').type('test@abc.com')
            .get('#genterWrapper > .col-md-9 > :nth-child(1)').click()
            .get('#userNumber').type('33333333')     
            .get('input[type="file"]').attachFile(filepath)
            .get('#submit').click()
            .get('.modal-body').should('not.be.visible')
    })

    it('Assert if Last name is a mandatory field', () => {
        cy.visit(Cypress.config().baseUrl + Cypress.config().formUrl)
            .get('#firstName').type('Test')
            .get('#userEmail').type('test@abc.com')
            .get('#genterWrapper > .col-md-9 > :nth-child(1)').click()
            .get('#userNumber').type('33333333')     
            .get('input[type="file"]').attachFile(filepath)
            .get('#submit').click()
            .get('.modal-body').should('not.be.visible')
    })

    it('Assert if Gender is a mandatory field', () => {
        cy.visit(Cypress.config().baseUrl + Cypress.config().formUrl)
            .get('#lastName').type('Test')
            .get('#firstName').type('Test')
            .get('#userEmail').type('test@abc.com')
            .get('#userNumber').type('33333333')     
            .get('input[type="file"]').attachFile(filepath)
            .get('#submit').click()
            .get('.modal-body').should('not.be.visible')
    })

    it('Assert if mobile number is a mandatory field', () => {
        cy.visit(Cypress.config().baseUrl + Cypress.config().formUrl)
            .get('#firstName').type('Test')
            .get('#lastName').type('Test')
            .get('#userEmail').type('test@abc.com')
            .get('#genterWrapper > .col-md-9 > :nth-child(1)').click()
            .get('input[type="file"]').attachFile(filepath)
            .get('#submit').click()
            .get('.modal-body').should('not.be.visible')
    })   

})
